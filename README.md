**Índice**  

 [[_TOC_]]  

# Best Practices Jenkins

## SLOTs de Ejecución

En Jenkins tendremos 4 slots de ejecución para procesos:
- `Slot 1. De 00:00 a 06:59`. Reservado para procesos que trabajen con ficheros del día en curso o sean procesos largos.
- `Slot 2. De 12:00 a 14:59`. Reservado para unas pocas ETLs que se encargan de realizar la capturan datos desde los diferentes orígenes de datos: FTPs, APIs, etc. Y tras la captura, los persisten en el BigData (SFTP) para que nuestros procesos los puedan procesar (valga la redundancia).
- `Slot 3. De 18:00 a 23:59`. Reservado para procesos que trabajen con ficheros del día anterior (current date – 1)
- `Slot 4. Real time`. Reservado para procesos de periodicidad inferior al día: 5 minutales, 10 minutales, horarios, etc

Todo proceso se debe ajustar a esos 4 slots estándars.

De esta manera conseguimos tener dos períodos libres:
- Entre las 7am y las 12am. Este período es de uso intensivo de negocio, con lo que conviene no tener carga en el sistema
- Entre las 3pm y las 6pm. Este período está reservado para actividades de Plataforma, como por ejemplo migraciones, instalación de parches, etc.


## Nombrado de los jobs de Jenkins

Para nombrar a los jobs de Jenkins se debe seguir la siguiente norma:
[SLOT]_[NOMBRE_ETL]

Donde [SLOT] puede valer uno de los siguientes valores:

- `a_slot1` -> Se corresponde con el slot 1, Desde las 00:00 a las 06:59
- `b_slot2` -> Se corresponde con el slot 2, Desde las 12:00 a las 14:59
- `c_slot3` -> Se corresponde con el slot 3, Desde las 18:00 a las 23:59
- `rt` -> Se corresponde con el slot 4, donde se ejecutan procesos realtime (5 mins, 10 mins, horarios, etc)
- `z_desactivado` -> Este prefijo lo tendrán todos los jobs inactivos.

Ejemplos
- a_slot1_py_energia_extraccion_api_datadis_consumos_municipales
- b_slot2_py_integracion_housekeeping
- c_slot3_etl_cdmge_gaco
- rt_etl_medioambiente_aire_10
- z_desactivado_c_slot3_etl_eurostat


Esta norma de nombrado consiste en tener  unos prefijos que ayuden a agrupar los Jobs de Jenkins todos juntos y que al final de la lista aparezcan los jobs inactivos.